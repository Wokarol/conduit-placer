﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wokarol.Grid;
using Wokarol.TouchSystem;

namespace Wokarol.DragSystem
{
    public class Draggable : MonoBehaviour, IDragable
    {
        [SerializeField] GridAsset grid = null;

        Vector2? offset = null;

        public void OnDrag(Vector2 pos, InteractionType type) {
            transform.position = (Vector2)(pos - offset);
        }

        public void OnDragStart(Vector2 startPos, InteractionType type) {
            offset = startPos - (Vector2)transform.position; 
        }

        public void OnDragStop(Vector2 endPos, InteractionType type) {
            transform.position = grid.SnapToGrid((Vector2)(endPos - offset));
            offset = null;
        }
    }
}
