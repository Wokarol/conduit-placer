﻿using UnityEngine;
using Wokarol.TouchSystem;

namespace Wokarol.DragSystem
{
    internal interface IDragable
    {
        void OnDragStart(Vector2 startPos, InteractionType type);
        void OnDrag(Vector2 pos, InteractionType type);
        void OnDragStop(Vector2 endPos, InteractionType type);
    }
}