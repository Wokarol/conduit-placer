﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wokarol.DragSystem;
using Wokarol.ClickSystem;
using System;

namespace Wokarol.TouchSystem
{
    [RequireComponent(typeof(Camera))]
    public class InteractionTrigger : MonoBehaviour
    {
        [SerializeField] float timeToDrag = 0.1f;

        InteractionTriggerHandler primaryHandler;
        InteractionTriggerHandler secondaryHandler;

        private void Awake() {
            Camera camera = GetComponent<Camera>();

            primaryHandler   = new InteractionTriggerHandler(0         , camera, InteractionType.Primary,   0);
            secondaryHandler = new InteractionTriggerHandler(timeToDrag, camera, InteractionType.Secondary, 1);
        }

        private void Update() {
            primaryHandler  .Update();
            secondaryHandler.Update();
        }

        private void OnValidate() {
            if (primaryHandler   != null) primaryHandler  .TimeToDrag = timeToDrag;
            if (secondaryHandler != null) secondaryHandler.TimeToDrag = timeToDrag;
        }
    } 

    class InteractionTriggerHandler
    {
        public float TimeToDrag { get; set; }
        public Camera Camera { get; protected set; }

        InteractionType type;
        int mouseButton;

        bool dragging = false;
        bool triedDragging = false;
        IDragable draggedObject = null;

        float timer = -1;

        public InteractionTriggerHandler(float timeToDrag, Camera camera, InteractionType type, int mouseButton) {
            TimeToDrag = timeToDrag;
            Camera = camera;
            this.type = type;
            this.mouseButton = mouseButton;
        }


        public void Update() {
            if (Input.GetMouseButtonDown(mouseButton)) {
                // On Press
                timer = 0;
                triedDragging = false;
            } else if (Input.GetMouseButton(mouseButton)) {
                // While holding
                if (!dragging && !triedDragging) {
                    timer += Time.deltaTime;
                    if (timer >= TimeToDrag) {
                        triedDragging = true;
                        StartDragging();
                    }
                } else if (dragging) {
                    ContinueDragging();
                }
            } else if (Input.GetMouseButtonUp(mouseButton)) {
                // On release
                if (dragging) StopDragging();
                if (!dragging && !triedDragging) Click();
            }
        }

        private bool Click() {
            var hit = Physics2D.GetRayIntersection(Camera.ScreenPointToRay(Input.mousePosition));
            if (hit.transform) {
                var clickObject = hit.transform.GetComponent<IClickable>();
                if (clickObject != null) {
                    clickObject.Trigger(type);
                    return true;
                }
            }
            return false;
        }

        private void StopDragging() {
            dragging = false;
            draggedObject.OnDragStop(Camera.ScreenToWorldPoint(Input.mousePosition), type);
            draggedObject = null;
        }

        private void ContinueDragging() {
            draggedObject.OnDrag(Camera.ScreenToWorldPoint(Input.mousePosition), type);
        }

        private bool StartDragging() {
            var hit = Physics2D.GetRayIntersection(Camera.ScreenPointToRay(Input.mousePosition));
            if (hit.transform) {
                draggedObject = hit.transform.GetComponent<IDragable>();
                if (draggedObject != null) {
                    dragging = true;
                    draggedObject.OnDragStart(Camera.ScreenToWorldPoint(Input.mousePosition), type);
                    return true;
                }
            }
            return false;
        }
    }
}
