﻿using Wokarol.TouchSystem;

namespace Wokarol.ClickSystem
{
    public interface IClickable
    {
        void Trigger(InteractionType type);
    } 
}
