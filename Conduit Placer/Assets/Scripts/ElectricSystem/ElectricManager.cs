﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wokarol.Grid;

namespace Wokarol.ElectricSystem
{
    public class ElectricManager : MonoBehaviour
    {
        [SerializeField] GridAsset grid;

        [Space]
        [SerializeField] Color inactiveColor = Color.white;
        [SerializeField] Color activeColor = Color.blue;

        public Color InactiveColor => inactiveColor;
        public Color ActiveColor => activeColor;

        Dictionary<Vector2Int, ElectricObject> electricGrid = new Dictionary<Vector2Int, ElectricObject>();

        public Dictionary<Vector2Int, ElectricObject> ElectricGrid => electricGrid;

        public void RemoveFromGrid(ElectricObject electricObject) {
            if (electricGrid.ContainsKey(electricObject.Coord) && electricGrid[electricObject.Coord] == electricObject) {
                electricGrid.Remove(electricObject.Coord);
                electricObject.SwitchGroup(null);
                DeleteConnections(electricObject);
            }
        }

        public void Place(ElectricObject electricObject) {
            Vector3 newElectricObjectPos = grid.SnapToGrid(electricObject.transform.position);
            Vector2Int newGridPos = new Vector2Int(Mathf.FloorToInt(newElectricObjectPos.x / grid.Scale), Mathf.FloorToInt(newElectricObjectPos.y / grid.Scale));

            if (!electricGrid.ContainsKey(newGridPos)) {
                //Debug.Log($":Core: Adding {electricObject} to grid on [{newGridPos.x}, {newGridPos.y}]");


                electricObject.transform.position = newElectricObjectPos;
                electricGrid.Add(newGridPos, electricObject);

                electricObject.Coord = newGridPos;

                var newGroup = new ElectricGroup();
                electricObject.SwitchGroup(newGroup);
                FindConnections(electricObject);
            }

        }

        private void DeleteConnections(ElectricObject electricObject) {
            var connections = electricObject.Connections;
            ElectricGroup groupToRebuild = null;

            if (connections.Up) LookforGroup(Vector2Int.up);
            if (connections.Down  || groupToRebuild != null) LookforGroup(Vector2Int.down);
            if (connections.Left  || groupToRebuild != null) LookforGroup(Vector2Int.left);
            if (connections.Right || groupToRebuild != null) LookforGroup(Vector2Int.right);

            if(groupToRebuild != null) {
                var groupObjects = new List<ElectricObject>(groupToRebuild.Objects);
                foreach(var ob in groupObjects) {
                    ob.SwitchGroup(new ElectricGroup());
                }
                foreach (var ob in groupObjects) {
                    FindConnections(ob);
                }
            }

            void LookforGroup(Vector2Int direction) {
                var newCoord = electricObject.Coord + direction;
                if (electricGrid.ContainsKey(newCoord) && electricGrid[newCoord].Connections.HasConnection(direction.Minus())) {
                    groupToRebuild = electricGrid[newCoord].Group;
                }
            }
        }

        private void FindConnections(ElectricObject electricObject) {
            var connections = electricObject.Connections;
            //Debug.Log($"Finding connections for {electricObject.gameObject.name} on [{electricObject.Coord}]");
            if (connections.Up) Connect(Vector2Int.up);
            if (connections.Down) Connect(Vector2Int.down);
            if (connections.Left) Connect(Vector2Int.left);
            if (connections.Right) Connect(Vector2Int.right);

            void Connect(Vector2Int direction) {
                var newCoord = electricObject.Coord + direction;
                if (electricGrid.ContainsKey(newCoord) && electricGrid[newCoord].Connections.HasConnection(direction.Minus())) {
                    //Debug.Log($"[{newCoord}] <color=green>Found connection</color>");
                    electricGrid[newCoord].Group.Overflow(electricObject.Group);
                } else {
                    //Debug.Log($"[{newCoord}] No connection");
                }
            }
        }
    } 
}
