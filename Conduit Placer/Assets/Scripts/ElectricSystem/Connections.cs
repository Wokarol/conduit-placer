﻿using System;
using UnityEngine;

namespace Wokarol.ElectricSystem
{
    [System.Serializable]
    public struct Connections
    {
        [SerializeField] bool up, right, left, down;

        public Connections(bool up, bool left, bool down, bool right) {
            this.up = up;
            this.right = right;
            this.left = left;
            this.down = down;
        }

        public bool Up => up;
        public bool Right => right;
        public bool Left => left;
        public bool Down => down;

        internal Connections Rotate(int z) {
            //Debug.Log($"Rotated {z} times");
            bool[] directions = new bool[4] { up, left, down, right };
            return new Connections(
                directions[(0 - z + 4)%4], 
                directions[(1 - z + 4)%4], 
                directions[(2 - z + 4)%4], 
                directions[(3 - z + 4)%4]);
        }

        public bool HasConnection(Vector2Int vector) {
            if(vector == Vector2Int.up) {
                return up;
            } else if(vector == Vector2Int.right) {
                return right;
            } else if (vector == Vector2Int.left) {
                return left;
            } else if (vector == Vector2Int.down) {
                return down;
            } else {
                Debug.LogError($"There is no {vector} direction");
                return false;
            }
        }
    }
}