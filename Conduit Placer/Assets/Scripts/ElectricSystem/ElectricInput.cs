﻿using UnityEngine;
using Wokarol.ClickSystem;
using Wokarol.TouchSystem;

namespace Wokarol.ElectricSystem
{
    internal class ElectricInput : ElectricObject , IClickable
    {
        [Header("Input Data")]
        [SerializeField] bool active = false;
        [SerializeField] SpriteRenderer[] inputSignalizers = new SpriteRenderer[0];

        public override bool IsPowered => active;

        private void Update() {
            foreach (var renderer in renderers) {
                if (group != null && renderer != null && base.IsPowered)
                    renderer.color = manager.ActiveColor;
                else
                    renderer.color = manager.InactiveColor;
            }
            foreach (var renderer in inputSignalizers) {
                if (group != null && renderer != null && IsPowered)
                    renderer.color = manager.ActiveColor;
                else
                    renderer.color = manager.InactiveColor;
            }
        }

        protected override void Register(ElectricGroup group) {
            base.Register(group);
            group.RegisterInput(this);
        }

        protected override void Unregister(ElectricGroup group) {
            base.Unregister(group);
            group.UnregisterInput(this);
        }

        public void Trigger(InteractionType type) {
            if (type == InteractionType.Secondary) {
                active = !active;
            }
        }
    }
}