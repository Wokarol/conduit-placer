﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Wokarol.ElectricSystem;

[CustomEditor(typeof(ElectricManager))]
public class ElectricManagerEditor : Editor
{
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        ElectricManager manager = target as ElectricManager;

        var keys = manager.ElectricGrid.Keys;
        List<string> objects = new List<string>();
        foreach (var key in keys) {
            objects.Add($"[{key.x}, {key.y}] {manager.ElectricGrid[key].gameObject.name} with coords: [{manager.ElectricGrid[key].Coord.x}, {manager.ElectricGrid[key].Coord.y}]");
        }
        

        EditorGUILayout.HelpBox(string.Join("\n", objects), MessageType.Info);
    }
}
