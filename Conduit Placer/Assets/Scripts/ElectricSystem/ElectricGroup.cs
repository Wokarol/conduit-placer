﻿using System.Collections.Generic;
using UnityEngine;

namespace Wokarol.ElectricSystem
{
    public class ElectricGroup
    {
        List<ElectricObject> objects = new List<ElectricObject>();
        List<ElectricInput> inputs = new List<ElectricInput>();

        public List<ElectricObject> Objects => objects;

        public bool HasPower { get {
                bool hasPower = false;
                foreach (var input in inputs) {
                    hasPower = hasPower || input.IsPowered;
                }
                return hasPower;
            } }

        public Color Color { get;} // Debug purposes

        public ElectricGroup() {
            Color = Color.HSVToRGB(Random.value, 0.8f, 1f);
        }

        internal void RegisterInput(ElectricInput electricInput) {
            if (!inputs.Contains(electricInput)) {
                inputs.Add(electricInput);
            }
        }

        internal void UnregisterInput(ElectricInput electricInput) {
            if (inputs.Contains(electricInput)) {
                inputs.Remove(electricInput);
            }
        }

        internal void Register(ElectricObject electricObject) {
            if (!objects.Contains(electricObject)) {
                objects.Add(electricObject);
            }
        }

        internal void Unregister(ElectricObject electricObject) {
            if (objects.Contains(electricObject)) {
                objects.Remove(electricObject);
            }
        }

        internal void Overflow(ElectricGroup group) {
            var oldObjectsList = new List<ElectricObject>(objects);
            foreach (var ob in oldObjectsList) {
                ob.SwitchGroup(group);
            }
        }
    }
}