﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wokarol.DragSystem;
using Wokarol.TouchSystem;

namespace Wokarol.ElectricSystem
{
    public class ElectricObject : MonoBehaviour, IDragable
    {
        [SerializeField] protected ElectricManager manager = null;
        [SerializeField] protected Connections preset = new Connections();
        [SerializeField] protected SpriteRenderer[] renderers = new SpriteRenderer[0];

        protected ElectricGroup group;

        public ElectricManager Manager { get { return manager; } set { manager = value; } }

        public ElectricGroup Group => group;
        public virtual bool IsPowered => group.HasPower;
        public Vector2Int Coord { get; set; }
        public Connections Connections => preset.Rotate((int)(transform.rotation.eulerAngles.z / 90));

        public void SwitchGroup(ElectricGroup newGroup) {
            if (group != null) {
                Unregister(group);
            }
            if (newGroup != null) {
                Register(newGroup);
            }
            group = newGroup;
        }

        void Update() {
            foreach (var renderer in renderers) {
                if (group != null && renderer != null && IsPowered)
                    renderer.color = manager.ActiveColor;
                else
                    renderer.color = manager.InactiveColor;
            }
        }

        protected virtual void Register(ElectricGroup group) {
            group.Register(this);
        }
        protected virtual void Unregister(ElectricGroup group) {
            group.Unregister(this);
        }


        // Dragging
        Vector2? offset = null;

        public void OnDrag(Vector2 pos, InteractionType type) {
            if (type != InteractionType.Primary) return;
            transform.position = (Vector2)(pos - offset);
        }

        public void OnDragStart(Vector2 startPos, InteractionType type) {
            if (type != InteractionType.Primary) return;
            offset = startPos - (Vector2)transform.position;
            manager.RemoveFromGrid(this);
        }

        public void OnDragStop(Vector2 endPos, InteractionType type) {
            if (type != InteractionType.Primary) return;
            transform.position = (Vector2)(endPos - offset);
            manager.Place(this);
            offset = null;
        }
    }
}
