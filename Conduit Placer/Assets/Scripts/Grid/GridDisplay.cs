﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wokarol.Grid
{
    [ExecuteInEditMode]
    public class GridDisplay : MonoBehaviour
    {
        [SerializeField] GridAsset grid = null;

        private void OnDrawGizmosSelected() {
            float scale = grid.Scale;
            Vector3 position = transform.position;
            for (int i = 0; i < 10; i++) {
                // Get every X line
                Vector3 start = grid.SnapToGrid(position + Vector3.right * i * scale);

                Debug.DrawRay(start, Vector3.up * 10.5f * scale);
                Debug.DrawRay(start, Vector3.down * 10.5f * scale);
            }
            for (int i = 0; i < 10; i++) {
                // Get every X line
                Vector3 start = grid.SnapToGrid(position + Vector3.left * i * scale);

                Debug.DrawRay(start, Vector3.up * 10.5f * scale);
                Debug.DrawRay(start, Vector3.down * 10.5f * scale);
            }

            for (int i = 0; i < 10; i++) {
                // Get every X line
                Vector3 start = grid.SnapToGrid(position + Vector3.up * i * scale);

                Debug.DrawRay(start, Vector3.right * 10.5f * scale);
                Debug.DrawRay(start, Vector3.left * 10.5f * scale);
            }
            for (int i = 0; i < 10; i++) {
                // Get every X line
                Vector3 start = grid.SnapToGrid(position + Vector3.down * i * scale);

                Debug.DrawRay(start, Vector3.right * 10.5f * scale);
                Debug.DrawRay(start, Vector3.left * 10.5f * scale);
            }
        }
    } 
}
