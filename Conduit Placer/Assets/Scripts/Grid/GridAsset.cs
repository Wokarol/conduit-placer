﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wokarol.Grid.Internal;

namespace Wokarol.Grid
{
    [CreateAssetMenu]
    public class GridAsset : ScriptableObject
    {
        [SerializeField] float scale = 1;
        [SerializeField] Vector2 offset = Vector2.zero;

        public float Scale => scale;
        public Vector2 Offset => offset;

        private GridHandler gridHandler;
        GridHandler GridHandler {
            get {
                if (gridHandler == null) gridHandler = new GridHandler();
                return gridHandler;
            }
        }

        public Vector3 SnapToGrid(Vector3 pos) {
            return GridHandler.SnapToGrid(pos, scale, offset);
        }
    }
}

namespace Wokarol.Grid.Internal
{
    public class GridHandler
    {
        public Vector2 SnapToGrid(Vector2 pos, float scale, Vector2 offset) {
            return new Vector2(
                Mathf.Round((pos.x - offset.x) / scale),
                Mathf.Round((pos.y - offset.y) / scale)
                ) * scale + offset;
        }
    } 
}
