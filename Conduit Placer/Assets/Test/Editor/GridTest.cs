﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wokarol.Grid.Internal;

namespace Wokarol.Grid.Test
{
    public class GridTest
    {
        GridHandler grid = new GridHandler();

        [Test]
        public void Snap_Scale_1() {
            Assert.AreEqual(new Vector2(-6, 5), grid.SnapToGrid(new Vector2(-5.7f, 4.8f), 1, Vector2.zero));
            Assert.AreEqual(new Vector2(6, 0), grid.SnapToGrid(new Vector2(5.6f, -0.2f), 1, Vector2.zero));
            Assert.AreEqual(new Vector2(-5, -15), grid.SnapToGrid(new Vector2(-5.2f, -14.7f), 1, Vector2.zero));
            Assert.AreEqual(new Vector2(90, 13), grid.SnapToGrid(new Vector2(90.3f, 12.9f), 1, Vector2.zero));
        }

        [Test]
        public void Snap_Scale_Not1() {
            Assert.AreEqual(new Vector2(-6, 4), grid.SnapToGrid(new Vector2(-5.7f, 4.8f), 2, Vector2.zero));
            Assert.AreEqual(new Vector2(4, 0), grid.SnapToGrid(new Vector2(5.6f, -0.2f), 4, Vector2.zero));
            Assert.AreEqual(new Vector2(-5, -14.5f), grid.SnapToGrid(new Vector2(-5.2f, -14.4f), 0.5f, Vector2.zero));
            Assert.AreEqual(new Vector2(90.4f, 13), grid.SnapToGrid(new Vector2(90.31f, 12.91f), 0.2f, Vector2.zero));
        }

        [Test]
        public void Snap_Scale_Not1_Offset() {
            Assert.AreEqual(new Vector2(-5, 4), grid.SnapToGrid(new Vector2(-5.7f, 4.8f), 2, Vector2.right));
            Assert.AreEqual(new Vector2(6, -1), grid.SnapToGrid(new Vector2(5.6f, -0.2f), 4, new Vector2(2, 3)));
        }
    } 
}
