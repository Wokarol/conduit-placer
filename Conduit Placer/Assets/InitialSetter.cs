﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wokarol
{
    public class InitialSetter : MonoBehaviour
    {
        [SerializeField] ElectricSystem.ElectricObject[] objects = new ElectricSystem.ElectricObject[0];
        [SerializeField] ElectricSystem.ElectricManager manager = null;

        private void Start() {
            foreach (var ob in objects) {
                ob.Manager = manager;
                manager.Place(ob);
            }
        }
    } 
}
