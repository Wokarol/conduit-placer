﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2IntUtils
{
    public static Vector2Int Minus(this Vector2Int vector) {
        return new Vector2Int(-vector.x, -vector.y);
    }
}
